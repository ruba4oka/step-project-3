## Задание

Вам нужно создать страницу, на которой Секретарша сможет создавать карточки, описывающие запланированные визиты к врачам.

На странице должны присутствовать:

1. Header (шапка) страницы:
  - в левом верхнем углу - логотип. Можно взять любой
  - в правом углу - Кнопка "Вход". После успешной авторизации она должна меняться на Кнопку "Создать визит".
2. Под Header - форма для фильтрации визитов. В этой форме должно быть 3 поля:
  - поиск по заголовку/содержимому визита
  - поиск по статусу (Open/Done) (визит прошел или еще нет)
  - срочность визита (High, Normal, Low)
3. Под формой фильтров - список созданных визитов.

#### Командная работа

На данном проекте все студенты разделены на группы по три человека. Студенты могут распределять между собой задачи самостоятельно. При сдаче проекта необходимо в файле `Readme.md` указать, кто выполнял какую часть задания.

#### Технические требования

- При первом посещении пользователем страницы, на доске должна быть надпись `No items have been added`. Эта же надпись должна быть, если у пользователя нету ни одной добавленной карточки (например, он их все удалил).
- По клику на кнопку **Вход** появляется модальное окно, в котором пользователь вводит свой email и пароль. Если он верный - пользователю на странице выводится список ранее созданных визитов.
- По клику на кнопку **Создать визит** появляется модальное окно, в котором можно создать новую карточку.
- Для создания классов нужно использовать синтаксис `class` из ES6.
- Для выполнения AJAX запросов можно использовать `fetch` или `axios`.
- После выполнения любых AJAX запросов, страница не должна перезагружаться. При добавлении/удалении карточки и других подобных операциях, с сервера **не должен** заново загружаться весь список карточек. Необходимо использовать данные из ответа сервера и Javascript для обновления информации на странице.
- При обновлении страницы или ее закрытии, ранее добавленные заметки не должны пропадать.
- Желательно разделить проект на модули с помощью ES6 modules.

##### Модальное окно "Создать визит"

В модальном окне должны присутствовать:

- Выпадающий список (select) с выбором врача. В зависимости от выбранного врача, под этим выпадающим списком будут появляться поля, которые нужно дозаполнить для визита к этому врачу.
- В выпадающем списке должно быть три опции - **Кардиолог**, **Стоматолог**, **Терапевт**.
- После выбора доктора из выпадающего списка, под ним должны появиться поля для записи к этому доктору. Несколько полей являются одинаковыми для всех трех докторов:
  - цель визита
  - краткое описание визита
  - выпадающее поле - срочность (обычная, приоритетная, неотложная)
  - ФИО
- Также, каждый из докторов имеет свои уникальные поля для заполнения. Если выбрана опция **Кардиолог**, дополнительно появляются следующие поля для ввода информации:
  - обычное давление
  - индекс массы тела
  - перенесенные заболевания сердечно-сосудистой системы
  - возраст
- Если выбрана опция **Стоматолог**, дополнительно необходимо заполнить:
  - дата последнего посещения
- Если выбрана опция Терапевт, дополнительно необходимо заполнить:
  - возраст
- Кнопка `Создать`. При клике на кнопку отправляется AJAX запрос на соответствующий адрес, и если в  ответе пришла информация о новосозданной карточке - создается карточка в Доске визитов на странице, модальное окно закрывается.
- Кнопка `Закрыть` - закрывает модальное окно без сохранения информации и создания карточки. По клику на область вне модального окна - модальное окно также закрывается.
- Все поля ввода, независимо от выбранной опции, кроме поля для дополнительных комментариев - обязательны для ввода данных. Валидацию на корректность данных делать не обязательно.

##### Карточка, описывающая визит

Карточка, которая создается по клику, появляется на доске задач. Это должно выглядеть примерно так:

![интерфейс](./img/2.png)

В ней должны присутствовать:
- ФИО, которые были введены при создании карточки
- Врач, к которому человек записан на прием
- Кнопка `Показать больше`. По клику на нее карточка расширяется, и появляется остальная информация, которая была введена при создании визита
- Кнопка `Редактировать`. При нажатии на нее, вместо текстового содержимого карточки появляется форма, где можно отредактировать введенные поля. Такая же, как в модальном окне при создании карточки
- Иконка с крестиком в верхнем правом углу, при нажатии на которую карточка будет удалена

##### Фильтры визитов

Фильтр карточек (поле input для ввода текста поиска по заголовку или описанию визита, выпадающий список по статусу, выпадающий список по приоритету) вам нужно делать на фронт-енде - то есть при изменении `value` любого элемента формы (выбран пункт в выпадающем списке, было введено что-то в `input`) вы фильтруете список ранее полученных с сервера карточек, и отображаете на экране новую информацию.

По принципу работы система должна быть похожа на фильтры в интернет-магазинах (например, слева [здесь](https://rozetka.com.ua/notebooks/c80004/)).

##### Классы

В JavaScript коде обязательно должны быть такие классы:
- класс Modal (всплывающее окно);
- класс Visit (описывающий общие для всех визитов к любому врачу поля и методы);
- дочерние классы VisitDentist, VisitCardiologist, VisitTherapist;

Методы и свойства каждого класса вам нужно продумать самостоятельно. При необходимости, вы можете добавлять также другие классы.

#### Требования к реализации

Дизайн может быть любой, но должен быть.

#### AJAX часть

Вся необходимая документация по взаимодействию с AJAX сервером находится [здесь](https://ajax.test-danit.com/api-pages/cards.html).

#### Необязательное задание продвинутой сложности

- При создании карточки визита, выполнять валидацию корректности введенных данных. Правила для валидации можно придумать самостоятельно (например, обычное давление должно являться числом, и находиться в диапазоне от 50 до 160)
- Добавить возможность пользователю перемещать карточки по доске методом Drag&Drop. Такие манипуляции с карточкой не влияют на месторасположение остальных карточек. После перетягивания карточки, не нужно "запоминать" ее новое местоположение. При перезагружке страницы она может вернуться на свое первоначальное место.




# Hospital task manager

## _Convinient visit task manager for hospitals_

The Hospital task management website is a simple, quick, and responsive website built with the
best principles of JavaScript Bootstrap technologies and Responsive Web Design.

## What is included

- A login mechanism that connects to the server and verifies the user's identity.
- Creating hospital visits by selecting a doctor and filling out the form's needed areas for that doctor.
- Render of any visits from the server that have been made and turning them into cards to display on the board.
- Unique filter which handles the card data and filters as selected by the user
- Cards offering the ability to modify, show more info or cancel a visit
- Fully responsive

## Tech

The Hospital task manager makes use of a variety of cutting-edge technology to make its job convenient:

- JavaScript
- Bootstrap
- HTML/CSS

## Project in detail:

- Requests class: [@levanton](https://github.com/levanton)

  - Functionality
    - Request module for (posting,getting,updating and deleting) data

- Modal class: [@levanton](https://github.com/levanton) [@mr-nazarii](https://github.com/mr-nazarii)

  - Functionality
    - Renders the Modal window
    - Renders the footer if selected

- LoginForm class: [@levanton](https://github.com/levanton)

  - Functionality
    - Renders the Login form
    - Verifies the validity of the inputted data

- VisitForm class: [@mr-nazarii](https://github.com/mr-nazarii) [@azapotochnyy](https://github.com/azapotochnyy)

  - Functionality
    - Renders the Visit form inside the Form
    - Opens the Form for modifications when called in the card

- Visit class: [@mr-nazarii](https://github.com/mr-nazarii) [@azapotochnyy](https://github.com/azapotochnyy)

  - Functionality
    - Renders the Visit form which has a select field with three options of doctors
    - Passes a set of standard required fields for filling after the doctor has been selected;
    - On selected option renders the chosen doctor form with passed standar questions for render

- VisitDentist VisitTherapist VisitCardiologist class: [@mr-nazarii](https://github.com/mr-nazarii) [@azapotochnyy](https://github.com/azapotochnyy)

  - Functionality
    - Renders the the fields of the selected doctor in the form

- Card class: [@azapotochnyy](https://github.com/azapotochnyy)

  - Functionality
    - Gets All the cards: sends GET reqest, maps data from response, saves json in a storrage, renders the visit cards on the board, if visit data is empty - shows: 'No items have been added';
    - Deletes the cards - sends a DELETE reqest, deletes: data from DOM, data from storrage. If the storrage is empty shows - 'No items have been added';
    - Modify the cards - modifies the Visit class to receive data from the cards when pressed modify, after prompts and fills in the Form with aquired data, then awaits for further modifications and does a Put request after the response - updates the DOM, storrage;
    - Show additional information - map data, shows information when the event is executed;

- Filter class: [@serhiisav](https://github.com/serhiisav)
  - Functionality
    - Creates a Filter Form and renders on the page:
      - Searches by title(purpose) / description(shortDesc)
      - Filters by status open/done (date of visit)
      - Filters by urgency (High/Medium/Low)
    - Gets all cards from local storage and compare with data from 'input' and(or) 'select' fields and show those that have matches. Gets visit status Appointment Date;
    - If no matches are found - renderes a message ('No results found');
    - Adds a button 'reset' - resets Filter Form and shows all cards.
